const blnk_line = """<div style="width:100%"></div>"""
const pointer_unicode = " ☛"
const nbsp = "&nbsp;"

function make_title_els(arr; n_pad_line=4)
    n = length(arr)
    l = maximum(length, arr)
    arr2 = if iseven(n)
        mid = floor(Int, n/2)
        vcat(arr[1:mid],
             repeat(nbsp, l+5) * pointer_unicode,
             arr[mid+1:end])
    else
        mid = floor(Int, n/2) + 1
        vcat(arr[1:mid-1],
             repeat(nbsp, l-length(arr[mid])+2) * arr[mid] * pointer_unicode,
             arr[mid+1:end])
    end
    return join(vcat(fill("", n_pad_line),
                     map(t -> """<div>$t</div>""", arr2),
                     fill("", n_pad_line)),
                blnk_line)
end

function make_index(photodirpath::String, titletx::String;
                    vh=vh_dflt, vw=vw_dflt,
                    batch_halfwidth=batch_halfwidth_dflt,
                    helpinfo=helpinfo,
                    credits=credits,
                    downloadinfo=downloadinfo)
    dircontent = readdir(photodirpath)
    photolist = filter(endswith(valid_formats), dircontent)
    zipfiles = filter(endswith(".zip"), dircontent)

    fotofrntnjs = read(fotofrntnjs_path, String)
    stylecss = read(stylecss_path, String)
    helpiconsvg = read(helpiconsvg_path, String)
    faviconpng = base64encode(read(faviconpng_path))
    swipejs = read(swipejs_path, String)

    titletx_pieces = split(titletx, "\\n")
    titletx_page = first(titletx_pieces)
    title_els = make_title_els(titletx_pieces)

    divs = map(enumerate(photolist)) do (i, photoid)
        datasrc = joinpath(photodirpath, photoid)
        return """<div id="$photoid" data-src="$datasrc"></div>"""
    end

    dls = map(zipfiles) do fname
        href = joinpath(photodirpath, fname)
        fsize = Base.format_bytes(filesize(href))
        return """<a href="$href" download="$fname">$downloadinfo $fname ($fsize)</a>"""
    end

    return """<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="$viewportcontent">
<title>$titletx_page</title>
<link rel="icon" type="image/png" href="data:image/png;base64,$faviconpng"/>
<style>
$stylecss
.swipe-wrap > div {
  float: left;
  position: relative;
  overflow: hidden;
  height: $(vh)vh;
  display: flex;
}
img {
  max-width: $(vw)vw;
  max-height: $(vh)vh;
  margin: auto;
}
video {
  max-width: $(vw)vw;
  max-height: $(vh)vh;
  margin: auto;
}
</style>
</head>
<body>
<div id="helpicon" class="tooltip hl">
  $helpiconsvg
  <div class="tooltiptext">$helpinfo</div>
</div>
<div style="padding:20px;"></div>
<div id="slider" class="swipe">
  <div id="wrap" class="swipe-wrap">
    <div id="title" style="visibility:hidden">$title_els</div>
    $(join(divs, "\n"))
  </div>
</div>
<div id="counter">
  <div id="counterhome" class="hl"><span>{ </span></div>
  <div id="counternums"></div>
  <div id="counterend"  class="hl"><span> }</span></div>
</div>
<div id="extras">
  <div>$(join(dls, "\n"))</div>
  <p>$credits</p>
</div>
<script>$swipejs</script>
<script>
const photolist = $photolist
const batch_halfwidth = $batch_halfwidth
$fotofrntnjs
</script>
</body>
</html>
"""
end

function make_index(photodirpath::String, titletx::Nothing; kwargs...)
    titletx = basename(photodirpath == "." ? pwd() : photodirpath)
    return make_index(photodirpath, titletx; kwargs...)
end

function make_index(argv::Dict)
    fr = argv["fr"]
    return make_index(argv["photodirpath"], argv["titletx"];
                      vh=argv["vh"], vw=argv["vh"],
                      batch_halfwidth=argv["batch_halfwidth"],
                      helpinfo=(fr ? helpinfo_fr : helpinfo),
                      credits=(fr ? credits_fr : credits),
                      downloadinfo=(fr ? downloadinfo_fr : downloadinfo))
end
