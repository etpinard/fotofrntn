module FotoFrntn

using RelocatableFolders
using ArgParse
using Base64

export get_argv, make_index

const valid_formats = r".jpg|.jpeg|.webp|.png|.mp4|.mov"

const fotofrntnjs_path = @path joinpath(@__DIR__, "..", "assets", "fotofrntn.js")
const stylecss_path = @path joinpath(@__DIR__, "..", "assets", "style.css")
const helpiconsvg_path = @path joinpath(@__DIR__, "..", "assets", "helpicon.svg")
const faviconpng_path = @path joinpath(@__DIR__, "..", "assets", "favicon.png")
const swipejs_path = @path joinpath(@__DIR__, "..", "deps", "swipe.min.js")

const hrefrepo = "https://gitlab.com/etpinard/fotofrntn"

const helpinfo = """
<p>Go to next photo by swiping ↩ / pressing → / enter / spacebar / pagedown</p>
<p>Go to previous photo by swiping ↪ / pressing ← / backspace / pageup </p>"""
const helpinfo_fr = """
<p>Photo suivante en glissant ↩ / pesant → / enter / espace / pagedown</p>
<p>Photo précédente en glissant ↪ / pesant ← / backspace / pageup</p>"""

const credits = """made with <a href="$hrefrepo" target="_blank">fotofrntn</a>"""
const credits_fr = """conçu avec <a href="$hrefrepo" target="_blank">fotofrntn</a>"""

const downloadinfo = "download"
const downloadinfo_fr = "télécharger"

const viewportcontent = "width=device-width, minimum-scale=1.0, maximum-scale=10.0"

include("args.jl")
include("make_index.jl")

end # module
