const vh_dflt = 85
const vw_dflt = 90
const batch_halfwidth_dflt = 1

function get_argv()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "photodirpath"
            help = "Path to photo directory."
            default = "."
        "--titletx", "-t"
            help = "Slide show title. Use `\\n` to generate multi-line titles."
        "--vh"
            help = "Sets the viewport height."
            arg_type = Int
            default = vh_dflt
        "--vw"
            help = "Sets the viewport width."
            arg_type = Int
            default = vw_dflt
        "--batch_halfwidth", "-b"
            help = "Sets the halfwidth of rendering batches triggered on swipe."
            arg_type = Int
            default = batch_halfwidth_dflt
        "--fr"
            help = "En français, SVP!"
            action = :store_true
    end
    return parse_args(s)
end
