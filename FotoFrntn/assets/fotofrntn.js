/* global photolist, batch_halfwidth */

const len = photolist.length
const querykey = "photoid="
const slideDelayFirstView = 20
const slideDelay = 200

const opts = {
  // disable infinite feel at endpoints
  continuous: false,
  // allow swipe via mouse drag interactions
  draggable: true,
  // append next image during swipe callback
  callback: function(ind, div, direction) {
    const photoid = div.id
    updateImgs(photoid)
    updateQuery(photoid)
    updateCounter(photoid)
  }
}

function getPhotoIndex(photoid) {
  const div = document.getElementById(photoid)
  return div ? Number(div.getAttribute("data-index")) : 0
}

function appendImg(div) {
  const img = document.createElement("img")
  img.src = div.getAttribute("data-src")
  div.appendChild(img)
}

function appendVideo(div) {
  const video = document.createElement("video")
  video.src = div.getAttribute("data-src")
  video.controls = true
  div.appendChild(video)
}

function isVideo(photoid) {
  return photoid.endsWith(".mp4") || photoid.endsWith(".mov")
}

function renderImgOrVid(photoid) {
  const div = document.getElementById(photoid)
  if (div && !div.firstChild) {
    if (isVideo(photoid)) {
      appendVideo(div)
    } else {
      appendImg(div)
    }
  }
  window.mySwipe.setup(opts)
}

function updateImgs(photoid) {
  const ind = getPhotoIndex(photoid)
  const i0 = Math.max(0, ind - batch_halfwidth)
  const i1 = i0 === 0 ? 2*batch_halfwidth : Math.min(ind + batch_halfwidth, len-1)
  for (let i = i0; i <= i1; i++) renderImgOrVid(photolist[i])
}

function updateQuery(photoid) {
  const loc = window.location
  const query = (photoid === "" || photoid === "title") ? "" : ("?" + querykey + photoid)
  const path = loc.protocol + "//" + loc.host + loc.pathname + query
  window.history.pushState({path: path}, "", path)
}

function toggleVisibility(id, cond) {
  const el = document.getElementById(id)
  el.style.visibility = cond ? "visible" : "hidden"
}

function updateCounter(photoid) {
  const counter = document.getElementById("counternums")
  while (counter.hasChildNodes()) counter.removeChild(counter.lastChild)
  const ind = getPhotoIndex(photoid)
  const tx = document.createTextNode(ind + " / " + len)
  counter.appendChild(tx)
  toggleVisibility("extras", ind === 0)
  toggleVisibility("counterhome", ind > 0)
  toggleVisibility("counterend", ind < len)
}

window.mySwipe = new Swipe(document.getElementById('slider'), opts)

function goToHome() {
  window.mySwipe.slide(0, slideDelay)
}

function goToEnd() {
  window.mySwipe.slide(len, slideDelay)
}

document.onkeydown = function(evt) {
  switch(evt.key) {
    case 'ArrowLeft':
    case 'Backspace':
    case 'PageUp':
      window.mySwipe.prev()
      break
    case 'ArrowRight':
    case 'Enter':
    case 'PageDown':
    case ' ':
      window.mySwipe.next()
      break
    case 'Home':
      goToHome()
      break
    case 'End':
      goToEnd()
      break
  }
}

document.getElementById("counterhome").onclick = goToHome
document.getElementById("counterend").onclick = goToEnd

// TODO
// - zoom via scroll

function firstView() {
  const query = window.location.search.slice(1)
  const photoid0 = query.split(querykey)[1] || ""
  updateImgs(photoid0)
  updateCounter(photoid0)
  window.mySwipe.slide(photolist.indexOf(photoid0) + 1, slideDelayFirstView)
}

// to avoid seeing a flickering title slide
setInterval(function() {
  const div = document.getElementById("title")
  div.style.visibility = null
}, slideDelayFirstView + 5)

firstView()
