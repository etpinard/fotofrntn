# fotofrntn

Simple photo front-end.

Creates an `index.html` for a directory of pictures with keyboard handlers
and swipe support via [`swipejs`](https://www.npmjs.com/package/swipejs).

Images are fetched just ahead of time, ensuring fast initial rendering and
avoiding unexpected data charges.

Each picture gets a unique URL (using a `?photoid=` query) making it easy to
share individual pictures.

## Demo

[![demo.gif](./demo.gif)](https://etpinard.gitlab.io/fotofrntn/)

Live demo available at: https://etpinard.gitlab.io/fotofrntn/

Big shoutout to https://picsum.photos for the pictures and
to https://jsoncompare.org/ for the mp4 video.

## Installation

Requires [Julia](https://julialang.org/downloads/), tested on version 1.6.1,
but should work on any version > 1.0, then

```
git clone https://gitlab.com/etpinard/fotofrntn.git
cd fotofrntn/FotoFrntn
julia -e 'import Pkg; Pkg.activate("."); Pkg.instantiate(); Pkg.build()'
cd ~/bin
ln -s path/to/fotofrntn/fotofrntn fotofrntn
```

## Usage

```
# show list of CLI options
fotofrntn --help

cd directory/of/pictures
fotofrntn > index.html

# or
fotofrntn directory/of/pictures > index.html
```

N.B. `fotofrntn` does not boot a server, e.g. use `python3 -m http.server`

## Behaviour

### Go to next photo

by swiping from right to left, or by pressing one of the right arrow, enter,
spacebar or page down keys.

### Go to previous photo

by swiping from left to right or by pressing one of the left arrow, backspace
or page up keys.

### Go to back to beginning

by pressing on the home key or by clicking on the `{` at the bottom right of the
screen.

### Go to last photo

by pressing on the end key or clicking on the `}` at the bottom right of the
screen.

## Optional features

### Link to downloadable zip archive

If the directory of pictures contains one or multiple ZIP archives,
`fotofrntn` will add downloadable links to them on the title page.

Some might find this useful to make available a higher resolution version of the
pictures that one wouldn't want to fetch on every swipe/keyboard interaction.

### Traduction en français

Utilise `fotofrntn --fr` !

## Related projects

- [thumbsup](https://thumbsup.github.io/), a better, more featureful, but more
  heavyweight solution (e.g. it requires `ffmpeg`).
- [home-gallery](https://github.com/xemle/home-gallery), also better more
  featureful, has a few anti-features like external API requests for
  Geo tags.


## License

[MIT](./LICENSE) - etpinard 2023
